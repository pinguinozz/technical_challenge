const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
var redis = require('redis');

const config = {
    GAMEID: 'game0001'
}
const service1 = require('./services/1_listen_and_push.js')(io, config)
const service2 = require('./services/2_subscribe_and_publish.js')(io, config)

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

server.listen(3000, function() {
    console.log('listening on *:3000');
});