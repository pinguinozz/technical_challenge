module.exports = function(io, config) {

    var RSMQWorker = require("rsmq-worker");

    var worker = new RSMQWorker(config.GAMEID, {
        interval: [.1, 1], // wait 100ms between every receive
        invisibletime: 2, // hide received message for 2 sec
        maxReceiveCount: 2, // only receive a message 2 times until delete
        autostart: true, // start worker on init
    });

    worker.on("message", function(msg, next, id) {
        // process the message
        console.log("[!] New message from queue. Id : " + id + ' - Msg: ' + msg);
        console.log("[!] Emitting:" + msg)
        io.emit('msg', msg)
        next()
    });

    worker.on('error', function(err, msg) {
        console.log("ERROR", err, msg.id);
    });
    worker.on('exceeded', function(msg) {
        console.log("EXCEEDED", msg.id);
    });
    worker.on('timeout', function(msg) {
        console.log("TIMEOUT", msg.id, msg.rc);
    });

    worker.start();

}

console.log('[!] Service 2 loaded')