var app = require('express')();
var redis = require('redis');
var pub = redis.createClient({
    'host': '127.0.0.1',
    'port': '6379'
});

module.exports = function(io, config) {

    RedisSMQ = require("rsmq");

    rsmq = new RedisSMQ({
        host: "127.0.0.1",
        port: 6379,
        ns: "rsmq"
    });

    rsmq.createQueue({
        qname: config.GAMEID
    }, function(err) {
        if (err) console.log('ERROR: ' + err)
    });

    io.on('connection', function(socket) {
        console.log('[!] User connected')
        socket.on('msg', function(data) {
            rsmq.sendMessage({
                qname: "game0001",
                message: data
            }, function(err, resp) {
                if (resp) {
                    console.log("[!] MESSAGE PUSHED TO QUEUE:", resp);
                }
            });


        });
    })
}


console.log('[!] Service 1 loaded')